﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public virtual Task<T> Create(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            entity.Id = Guid.NewGuid();
            var list = Data.ToList();
            list.Add(entity);
            Data = list;
            
            return Task.FromResult(entity);
        }

        public virtual Task<T> Update(T entity)
        {
            if(entity == null)
                throw new ArgumentNullException(nameof(entity));
            
            var entry = Data.Where(x => x.Id == entity.Id).FirstOrDefault();
            if(entry == null)
                return Create(entity);

            var list = Data.ToList();
            var index = list.IndexOf(list.FirstOrDefault(x=> x.Id == entity.Id));

            list[index] = entity;
            Data = list;
            return Task.FromResult(entity);            
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            var list = Data.FirstOrDefault(x => x.Id == id);
            Data = Data.Where(x => x.Id != id);            
            return Task.FromResult(list != null);
        }
    }
}